<!DOCTYPE html>
<html>
<head>
	<title>Bens Table</title>
</head>
<body>
	<table border="1" cellspacing="3" cellpadding="25" width="700" align="center">

		<tr bgcolor="green" align="center">
			<th>Name</th>
			<th>Age</th>
			<th colspan="2">Location</th>
		</tr>

		<tr align="center">
			<td rowspan="2">Aziz</td>
			<td rowspan="2">26</td>
			<td>Chicago</td>
			<td>IL</td>
		</tr>

		<tr align="center">
			<td>San diego</td>
			<td>CA</td>
		</tr>

		<tr align="center">
			<td>Jen</td>
			<td>40</td>
			<td>New York</td>
			<td>NY</td>
		</tr>

		<tr align="center">
			<td>Jordan</td>
			<td>20</td>
			<td>London</td>
			<td>UK</td>
		</tr>

		<tr>
			<td colspan="4" align="center" bgcolor="blue">Footer</td>
		</tr>

	</table>
</body>
</html>