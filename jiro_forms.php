<!DOCTYPE html>
<html>
<head>
	<title>Bens forms</title>
</head>
<body>
	<form>
		<table cellspacing="7" align="center">

			<tr>
				<th colspan="3"><h2>SEND US A MESSAGE</h2><hr></th>
			</tr>

			<tr>
				<td>
					<input type="text" name="username" placeholder="Full Name" >
				</td>

				<td>
					<input type="email" name="email" placeholder="Email Address">
				</td>

				<td>
					<input type="text" name="subject" placeholder="Subject">
				</td>
			</tr>

			<tr>
				<td colspan="3"><textarea cols="60" rows="10" placeholder="Message Body"></textarea></td>
			</tr>

			<tr>
				<td>
					<button name="submit" type="submit">Send</button>
				</td>
			</tr>
			
		</table>
	</form>
</body>
</html>