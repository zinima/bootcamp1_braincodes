<!DOCTYPE html>
<html>
<head>
	<title>Tables without CSS</title>
</head>
<body>
	<table border="1" width="400">
		<tr>
			<th>Name</th>
			<th>Age</th>
			<th colspan="2">Location</th>
		</tr>

		<tr>
			<td rowspan="2">Aziz</td>
			<td rowspan="2">26</td>
			<td>Chicago</td>
			<td>IL</td>
			<tr>
				<td>San Diego</td>
				<td>CA</td>
			</tr>
		</tr>

		<tr>
			<td>Jen</td>
			<td>40</td>
			<td>New York</td>
			<td>NY</td>
		</tr>

		<tr>
			<td>Jordan</td>
			<td>29</td>
			<td>London</td>
			<td>UK</td>
		</tr>

		<tr>
			<td colspan="4">Footer</td>
		</tr>
	</table>

</body>
</html>