<?php

class User{

	public $firstName;
	public $lastName;
	


	public function __construct($firstName, $lastName){
		$this->firstName = $firstName;
		$this->lastName = $lastName;
		
	}

	public function hello(){

		return "My  first name is " . $this->firstName . " and my last name is " . $this->lastName . ". Nice to meet you!";
	}
}


$user1 = new User("Zinima", "Pongon");

echo $user1->hello();	

?>