<?php

/**
* User Class with access modifier
*/
class User
{
	private $firstName;

	public function setName($firstName){

		$this->firstName = $firstName;

	}

	public function getName(){
		return $this->firstName;
	}
}

	$user1 = new User();

	$user1->setName("Joe");

	echo $user1->getName();
?>